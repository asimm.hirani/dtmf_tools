///! Fast and easy DTMF decoding using rustfft
///!
///! Supports generation of arbitrary long key press audio and decoding of DTMF
///! key presses.
/// 
/// 
use rustfft::{FftPlanner, num_complex::*, num_traits::FromPrimitive, Fft};
use std::{collections::BinaryHeap, sync::Arc};
use ordered_float::NotNan;

fn fft_idx_to_freq(idx : usize, nsamples : usize, sampling_freq : f64) -> f64 {
    idx as f64 / nsamples as f64 * sampling_freq
}

fn maximum<T : PartialOrd + Ord + Default>(arr : &[T], n : usize) -> Box<[(&T, usize)]> {
    let mut beep = BinaryHeap::with_capacity(arr.len());
    for (i, elem) in arr.iter().enumerate() {
        beep.push((elem, i));
    }
    let mut n = n;
    let mut vec_of_max_elems = Vec::with_capacity(arr.len());
    for elem in beep.drain() {
        vec_of_max_elems.push(elem);
        if n == 0 {
            break;
        }
        n -= 1;
    }
    vec_of_max_elems.into_boxed_slice()
}

/// Abstraction of a DTMF key.
/// 
/// Each enum element represents a key press either decoded or encoded.
/// 
/// Keys can be `play`ed using a `ToneGenerator`. Keys can also be turned into 
/// DTMF frequency pairs or deduced from a pair of frequencies using 
/// `get_dtmf_freq_pair` and `from_freqs` respectively.
/// 
///
/// ```
/// // Creates a new key
/// use dtmf_tools::Key;
/// fn main() {
///     let my_key = Key::Key1;
/// }
/// 
/// ```
#[derive(Debug, PartialEq)]
pub enum Key {
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    Key8,
    Key9,
    KeyStar,
    Key0, 
    KeyPound,
    KeyA,
    KeyB,
    KeyC,
    KeyD,
}

impl Key {
    /// Obtains the frequency pair associated with the Key
    pub fn get_dtmf_freq_pair(&self) -> [f64; 2] {
        match self {
            Key::Key1 => [697.0, 1209.0],
            Key::Key2 => [697.0, 1336.0],
            Key::Key3 => [697.0, 1447.0],
            Key::Key4 => [770.0, 1209.0],
            Key::Key5 => [770.0, 1336.0],
            Key::Key6 => [770.0, 1477.0],
            Key::Key7 => [852.0, 1209.0],
            Key::Key8 => [852.0, 1336.0],
            Key::Key9 => [852.0, 1447.0],
            Key::KeyStar => [941.0, 1209.0],
            Key::Key0 => [941.0, 1336.0],
            Key::KeyPound => [941.0, 1477.0],
            Key::KeyA => [697.0, 1633.0],
            Key::KeyB => [770.0, 1633.0],
            Key::KeyC => [852.0, 1633.0],
            Key::KeyD => [941.0, 1633.0],
        }
    }

    /// Performs a fuzzy match on the frequency pair provided and returns a key
    /// if the frequency pair is close. If a pair is not close, returns `None`
    pub fn from_freqs(freqs : &[f64; 2]) -> Option<Key> {
        let mut freqs = *freqs;
        if freqs[0] > freqs[1] {
            freqs.swap(1, 0);
        }
        let freqs = [fuzzy_freqs(freqs[0]) as i32, fuzzy_freqs(freqs[1]) as i32];
        match freqs {
            [697, 1209] => Some(Key::Key1),
            [697, 1336] => Some(Key::Key2),
            [697, 1447] => Some(Key::Key3),
            [770, 1209] => Some(Key::Key4),
            [770, 1336] => Some(Key::Key5),
            [770, 1477] => Some(Key::Key6),
            [852, 1209] => Some(Key::Key7),
            [852, 1336] => Some(Key::Key8),
            [852, 1447] => Some(Key::Key9),
            [941, 1209] => Some(Key::KeyStar),
            [941, 1336] => Some(Key::Key0),
            [941, 1477] => Some(Key::KeyPound),
            [697, 1633] => Some(Key::KeyA),
            [770, 1633] => Some(Key::KeyB),
            [852, 1633] => Some(Key::KeyC),
            [941, 1633] => Some(Key::KeyD),
            _ => None
        }
    }

    /// Plays a key using a given `transformer`
    pub fn play(&self, duration : f64, transformer : &ToneTransformer) -> Box<[f64]> {
        let freqs = self.get_dtmf_freq_pair();
        transformer.play_tone_list(
            &[Tone::with_frequency(freqs[0]), Tone::with_frequency(freqs[1])], 
            duration
        )
    }
}

fn fuzzy_freqs(freq : f64) -> f64 {
    const TOLERANCE : f64 = 30.0; // Hz
    const CENTER_FREQS : &[f64] = &[
        697.0,
        770.0,
        852.0,
        941.0,
        1209.0,
        1336.0,
        1477.0,
        1633.0
    ];
    for &center in CENTER_FREQS {
        if center + TOLERANCE > freq && center - TOLERANCE < freq {
            return center;
        }
    }
    0.0
}

pub struct Tone {
    freq : f64 // in Hz
}

impl Tone {
    pub fn with_frequency(freq : f64) -> Tone {
        Tone { freq }
    }
}

/// A [`ToneTransformer`] is used to convert a [`Tone`] or list of them into a 
/// series of [`f64`] samples. 
pub struct ToneTransformer {
    sample_rate : u32 // Samples per second
}

impl ToneTransformer {
    /// The main constructor of this struct. The `sample_rate` is the rate at
    /// which the true waveform will be sampled.
    pub fn with_sample_rate(sample_rate : u32) -> ToneTransformer {
        ToneTransformer { sample_rate }
    }

    /// This method will play a given [`Tone`] for a `duration` as specified
    /// at the [`ToneTransformer`]'s sampling rate. Amplitude is normalized to
    /// [0.0, 1.0]
    pub fn play(&self, to_play : &Tone, duration : f64) -> Box<[f64]> {
        let mut buffer = Vec::with_capacity(
            (self.sample_rate as f64 * duration).ceil() as usize
        );

        for sample_number in 0..buffer.capacity() {
            let t : f64 = sample_number as f64 / self.sample_rate as f64;
            let argument = to_play.freq * std::f64::consts::PI * 2.0 * t;
            let sample_float = argument.sin();
            buffer.push(sample_float);
        }
        buffer.into_boxed_slice()
    }

    /// Plays a list of [`Tone`]s. Will sum the overtones and normalize to the 
    /// range [0.0, 1.0]. 
    pub fn play_tone_list(&self, tone_list : &[Tone], duration : f64) -> Box<[f64]> {
        let mut buffer = Vec::with_capacity(
            (self.sample_rate as f64 * duration).ceil() as usize
        );
        for sample_number in 0..buffer.capacity() {
            let t : f64 = sample_number as f64 / self.sample_rate as f64;
            let sample : f64 = tone_list.iter().map( |tone | {
                (tone.freq * std::f64::consts::PI * 2.0 * t).sin()/tone_list.len() as f64
            }).sum();
            buffer.push(sample);
        }
        buffer.into_boxed_slice()
    }
}

/// API for decoding a sample buffer into a list of key-presses.
pub struct ToneDecoder {
    active_fft : Arc<dyn Fft<f64>>,
    block_size : usize,
    sampling_freq : f64
}

impl ToneDecoder {
    /// Generates an instance for a given sampling frequency to ensure
    /// optimal FFT bin size performance.
    pub fn for_sampling_frequency(sampling_freq : f64) -> ToneDecoder {
        // What block size do we need for a given frequency to resolve
        // 40 Hz bins?
        let block_size = sampling_freq as usize/40;
        let mut planner = FftPlanner::new();
        let fft = planner.plan_fft_forward(block_size);
        
        ToneDecoder { active_fft: fft , block_size, sampling_freq}
    }

    /// Decodes a signal into a series of key presses that were detected in the buffer
    /// in the order they were detected.
    pub fn decode_signal(&self, signal : &[f64]) -> Box<[Key]> {
        let subdiv_signal = subdivide(signal, self.block_size);
        let mut key_vector = Vec::with_capacity(subdiv_signal.len());
        for segment in subdiv_signal {
            key_vector.push(self.decode_segment(segment));
        }
        key_vector.into_boxed_slice()
    }

    fn decode_segment(&self, segment : &[f64]) -> Key {
        let mut cpv = Vec::with_capacity(segment.len());
        for &sample in segment {
            cpv.push(Complex64::from_f64(sample).unwrap());
        }
        self.active_fft.process(&mut cpv);
        let vec_of_magnitudes : Vec<NotNan<f64>> = cpv.iter().map(|elem| {
            NotNan::from(elem.abs()/(cpv.len() as f64))
        }).collect();
        
        let vec_of_magnitudes = &vec_of_magnitudes[0..vec_of_magnitudes.len()/2];

        let max_freqs = maximum(vec_of_magnitudes, 2);

        let peak_freqs = [
            fft_idx_to_freq(max_freqs[0].1, segment.len(), self.sampling_freq),
            fft_idx_to_freq(max_freqs[1].1, segment.len(), self.sampling_freq)
        ];

        Key::from_freqs(&peak_freqs).unwrap()

    }
}

fn subdivide(signal : &[f64], window_size : usize) -> Vec<&[f64]> {
    let n_windows = signal.len()/window_size;
    let mut windows : Vec<&[f64]> = Vec::with_capacity(n_windows);
    for window in 0..n_windows {
        if window != n_windows-1 {
            windows.push(&signal[window*window_size..(window+1)*window_size]);
        } else {
            break; // Somewhat bad practice, but we will live with it because we have enough samples likely.
        }
    }

    windows

}

#[cfg(test)]
mod tests {
    use super::*;

    const TOLERANCE : f64 = 0.00001;

    #[test]
    fn test_tone_gen_correctness() {
        // Create a tone
        let tone = Tone::with_frequency(1.0); // 1 Hz tone
        // Create a generator
        let generator = ToneTransformer::with_sample_rate(8); // 8 samples per cycle
        // Generate the sample
        let sample_list = generator.play(&tone, 1.0); // 1 second
        // Generate the answer array
        let answer_f64: [f64; 8] = [
                                    0.0, 
                                    2.0_f64.sqrt()/2.0, 
                                    1.0, 
                                    2.0_f64.sqrt()/2.0, 
                                    0.0, 
                                    -(2.0_f64.sqrt())/2.0, 
                                    -1.0, 
                                    -(2.0_f64.sqrt())/2.0
                                ];
        // Assert the correct length
        assert_eq!(sample_list.len(), answer_f64.len());
        // Assert each of the elements is close to the answer within tol
        for idx in 0..sample_list.len() {
            assert!((sample_list[idx]-answer_f64[idx]).abs() < TOLERANCE)
        }
    }

    #[test]
    fn test_key_freq() {
        let generator = ToneTransformer::with_sample_rate(20_000);
        let to_play = Key::Key0;
        let generated_samples = to_play.play(
            1.0, 
            &generator
        );

        let decoder = ToneDecoder::for_sampling_frequency(20_000_f64);
        let detected_keys = decoder.decode_signal(&generated_samples);

        for key in Vec::from(detected_keys) {
            assert_eq!(key, Key::Key0);
        }
    }

    #[test]
    fn test_key_freq_odd_signal_len() {
        let generator = ToneTransformer::with_sample_rate(20_000);
        let to_play = Key::Key0;
        let generated_samples = to_play.play(
            1.649393021102, 
            &generator
        );

        let decoder = ToneDecoder::for_sampling_frequency(20_000_f64);
        let detected_keys = decoder.decode_signal(&generated_samples);

        for key in Vec::from(detected_keys) {
            assert_eq!(key, Key::Key0);
        }
    }
}
