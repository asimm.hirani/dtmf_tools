# dtmf-tools

A Rust library to simplify common DTMF encoding/decoding schemes.

## Overview

`dtmf-tools` is a rustfft based library that simplifies some common DTMF encoding/decoding tasks. It features abstractions that simplify the following tasks:

- Pressing a DTMF key and generating their corresponding waveforms
- Decoding a pressed key into a waveform.

## Example

A basic waveform generation example with `dtmf-tools`

```rust
use dtmf_tools::{Key, ToneGenerator};

fn main() {
    // Generate a ToneTransformer with the sample rate you want
    let generator = ToneTransformer::with_sample_rate(20_000);
    // Choose the key you want to play
    let to_play = Key::Key0;
    let generated_samples = to_play.play(
        1.0, // The duration in seconds
        &generator // The generator with the sampling rate you want
    );
}
```

Basic waveform decoding:

```rust
use dtmf_tools::ToneDecoder;

fn main() {
    let samples : &[f64] = your_sample_source();

    // Initialize the Decoder with the sampling frequency of your source
    let decoder = ToneDecoder::for_sampling_frequency(20_000_f64);
    // Decode the signal
    let detected_keys = decoder.decode_signal(&generated_samples);
    // Iterate over the set of keys read
    for key in detected_keys {
        println!("Key Pressed: {:?}", key);
    }
}
```